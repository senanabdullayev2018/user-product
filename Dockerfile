FROM alpine
RUN apk add --no-cache openjdk17
COPY build/libs/user-product-1-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app
ENTRYPOINT ["java"]
CMD ["-jar", "user-product-1-0.0.1-SNAPSHOT.jar"]