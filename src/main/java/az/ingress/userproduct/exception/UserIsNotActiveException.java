package az.ingress.userproduct.exception;

import java.io.Serial;

public class UserIsNotActiveException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -1642337255871772172L;

    public UserIsNotActiveException(String message) {
        super(message);
    }
}
