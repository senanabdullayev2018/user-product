package az.ingress.userproduct.exception;

import java.io.Serial;

public class UserNotFoundException extends NotFoundException {
    @Serial
    private static final long serialVersionUID = -1642337255871772172L;

    public UserNotFoundException(String message) {
        super(message);
    }
}
