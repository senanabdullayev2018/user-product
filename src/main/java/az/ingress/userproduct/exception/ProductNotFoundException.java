package az.ingress.userproduct.exception;

import java.io.Serial;

public class ProductNotFoundException extends NotFoundException {
    @Serial
    private static final long serialVersionUID = -1642337255871772172L;

    public ProductNotFoundException(String message) {
        super(message);
    }
}
