package az.ingress.userproduct.exception;

import java.io.Serial;

public class ZeroBalanceException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -1642337255871772172L;

    public ZeroBalanceException(String message) {
        super(message);
    }
}
