package az.ingress.userproduct.exception;

import lombok.Getter;

import java.io.Serial;

@Getter
public class NotFoundException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 58432132465811L;
    private Object[] args;

    public NotFoundException(String message) {
        super(message);
    }

}
