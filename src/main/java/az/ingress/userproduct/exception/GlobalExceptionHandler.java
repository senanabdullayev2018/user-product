package az.ingress.userproduct.exception;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static az.ingress.userproduct.util.HttpResponseConstants.*;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends DefaultErrorAttributes {
    private static final String INTERNAL_SERVER_ERROR = "Internal server exception";

    private final MessageSource errorMessageSource;

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Map<String, Object>> handle(NotFoundException ex, WebRequest request, Locale locale) {
        log.info("Not found exception [message:{}, args:{}]", ex.getMessage(), ex.getArgs());
        return ofType(request, HttpStatus.NOT_FOUND, getLocalizedMessage(ex.getMessage(), ex.getArgs(), locale));
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    public final ResponseEntity<Map<String, Object>> handle(MissingServletRequestParameterException ex,
                                                            WebRequest request, Locale locale) {
        log.error("Missing servlet request parameter exception", ex);
        return ofType(request, HttpStatus.BAD_REQUEST, getLocalizedMessage(ex.getMessage(), locale));
    }


    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Map<String, Object>> handle(Exception ex, WebRequest request, Locale locale) {
        log.error(INTERNAL_SERVER_ERROR, ex);
        return ofType(request, HttpStatus.INTERNAL_SERVER_ERROR,
                getLocalizedMessage(HttpStatus.INTERNAL_SERVER_ERROR.name(), locale));
    }


    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message) {
        return ofType(request, status, message, Collections.emptyList());
    }

    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, HttpStatus status, String message,
                                                       List<ConstraintsViolationError> errors) {
        Map<String, Object> attributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put(STATUS, status.value());
        attributes.put(ERROR, status.getReasonPhrase());
        attributes.put(MESSAGE, message);
        attributes.put(ERRORS, errors);
        attributes.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, status);
    }

    private String getLocalizedMessage(String code, Locale locale) {
        return getLocalizedMessage(code, null, locale);
    }

    private String getLocalizedMessage(String code, Object[] arguments, Locale locale) {
        return errorMessageSource.getMessage(code, arguments, code, locale);
    }

}
