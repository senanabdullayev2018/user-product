package az.ingress.userproduct.service.impl;

import az.ingress.userproduct.dao.entity.User;
import az.ingress.userproduct.dao.repository.UserRepository;
import az.ingress.userproduct.exception.UserNotFoundException;
import az.ingress.userproduct.mapper.UserMapper;
import az.ingress.userproduct.model.request.UserRequest;
import az.ingress.userproduct.model.response.UserResponse;
import az.ingress.userproduct.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public Long createUser(UserRequest request) {
        log.info("createUser.start username: {}", request.getUsername());
        var user = UserMapper.INSTANCE.mapUserRequestToUser(request);
        var savedUser = userRepository.save(user);
        log.info("createUser.end username: {}", request.getUsername());
        return savedUser.getId();
    }

    @Override
    public void updateUser(Long id, UserRequest request) {
        log.info("updateUser.start id: {}", id);
        var user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User not found!"));
        UserMapper.INSTANCE.updateUser(user, request);
        userRepository.save(user);
        log.info("updateUser.end id: {}", id);
    }

    @Override
    public void updateUserActiveStatus(HashMap<String, Boolean> map) {
        log.info("updateUserActiveStatus.start!");
        List<User> usersToUpdate = new ArrayList<>();
        map.forEach((username, isActive) -> {
            var userOptional = userRepository.findByUsername(username);
            userOptional.ifPresent(user -> {
                user.setIsActive(isActive);
                usersToUpdate.add(user);
            });
        });
        userRepository.saveAll(usersToUpdate);
        log.info("updateUserActiveStatus.end!");
    }

    @Override
    public UserResponse getUserById(Long id) {
        log.info("getUserById.start id: {}", id);
        var user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User not found!"));
        log.info("getUserById.end id: {}", id);
        return UserMapper.INSTANCE.mapUserToUserResponse(user);
    }

    @Override
    public UserResponse getUserByUsername(String username) {
        log.info("getUserByUsername.start username: {}", username);
        var user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("User not found!"));
        log.info("getUserByUsername.end username: {}", username);
        return UserMapper.INSTANCE.mapUserToUserResponse(user);
    }

    @Override
    public List<UserResponse> getUsersByActivityStatus(Boolean isActive) {
        log.info("getActiveUsers.start!");
        var userList = userRepository.findByIsActiveIs(isActive);
        log.info("getActiveUsers.end!");
        return UserMapper.INSTANCE.mapUserListToUserResponseList(userList);
    }

    @Override
    public List<UserResponse> getAllUsers() {
        log.info("getUsers.start!");
        var userList = userRepository.findAll();
        log.info("getUsers.end!");
        return UserMapper.INSTANCE.mapUserListToUserResponseList(userList);
    }
}
