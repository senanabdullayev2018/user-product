package az.ingress.userproduct.service.impl;

import az.ingress.userproduct.dao.repository.ProductRepository;
import az.ingress.userproduct.exception.*;
import az.ingress.userproduct.mapper.ProductMapper;
import az.ingress.userproduct.mapper.UserMapper;
import az.ingress.userproduct.model.request.ProductRequest;
import az.ingress.userproduct.model.request.PurchaseRequest;
import az.ingress.userproduct.model.response.ProductResponse;
import az.ingress.userproduct.model.response.UserResponse;
import az.ingress.userproduct.service.ProductService;
import az.ingress.userproduct.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final UserService userService;
    private final ProductRepository productRepository;

    @Override
    public Long createProduct(ProductRequest request) {
        log.info("createProduct.start product name: {}", request.getName());
        var product = ProductMapper.INSTANCE.mapProductRequestToProduct(request);
        var savedProduct = productRepository.save(product);
        log.info("createProduct.end product name: {}", request.getName());
        return savedProduct.getId();
    }

    @Override
    public void updateProduct(Long productId, ProductRequest request) {
        log.info("updateProduct.start product id: {}", productId);
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException("Product not found!"));
        ProductMapper.INSTANCE.updateProduct(product, request);
        productRepository.save(product);
        log.info("updateProduct.end product id: {}", productId);
    }

    @Override
    public void deleteProduct(Long id) {
        log.info("deleteProduct.start product id: {}", id);
        var product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product not found!"));
        productRepository.delete(product);
        log.info("deleteProduct.end product id: {}", id);
    }

    @Override
    public ProductResponse getProductById(Long id) {
        log.info("getProductById.start id: {}", id);
        var product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException("Product not found!"));
        log.info("getProductById.end id: {}", id);
        return ProductMapper.INSTANCE.mapProductToProductResponse(product);
    }

    @Override
    public List<ProductResponse> getAllProducts() {
        log.info("getProductList.start!");
        var productList = productRepository.findAll();
        log.info("getProductList.end!");
        return ProductMapper.INSTANCE.mapProductListToProductResponseList(productList);
    }

    @Override
    public void buyProduct(PurchaseRequest purchaseRequest) {
        log.info("buyProduct.start! userId: {}, productId: {}, count: {}",
                purchaseRequest.getUserId(), purchaseRequest.getProductId(), purchaseRequest.getCount());
        var userResponse = userService.getUserById(purchaseRequest.getUserId());
        var productResponse = getProductById(purchaseRequest.getProductId());
        validateUserAndBalanceForPurchase(userResponse, productResponse, purchaseRequest.getCount());
        var totalAmount = productResponse.getPrice().multiply(BigDecimal.valueOf(purchaseRequest.getCount()));
        decreaseBalance(userResponse, purchaseRequest.getUserId(), totalAmount);
        decreaseStockCount(productResponse, purchaseRequest.getProductId(), purchaseRequest.getCount());
        log.info("buyProduct.end! userId: {}, productId: {}, count: {}",
                purchaseRequest.getUserId(), purchaseRequest.getProductId(), purchaseRequest.getCount());
    }

    @Override
    public void returnProduct(PurchaseRequest purchaseRequest) {
        log.info("returnProduct.start! userId: {}, productId: {}, count: {}",
                purchaseRequest.getUserId(), purchaseRequest.getProductId(), purchaseRequest.getCount());
        var userResponse = userService.getUserById(purchaseRequest.getUserId());
        var productResponse = getProductById(purchaseRequest.getProductId());
        if (!userResponse.getIsActive()) throw new UserIsNotActiveException("User is not active!");
        var totalAmount = productResponse.getPrice().multiply(BigDecimal.valueOf(purchaseRequest.getCount()));
        increaseBalance(userResponse, purchaseRequest.getUserId(), totalAmount);
        increaseStockCount(productResponse, purchaseRequest.getProductId(), purchaseRequest.getCount());
        log.info("returnProduct.end! userId: {}, productId: {}, count: {}",
                purchaseRequest.getUserId(), purchaseRequest.getProductId(), purchaseRequest.getCount());
    }

    private void validateUserAndBalanceForPurchase(UserResponse user, ProductResponse product, Integer count) {
        if (!user.getIsActive()) throw new UserIsNotActiveException("User is not active!");
        if (user.getBalance().compareTo(BigDecimal.ZERO) <= 0) throw new ZeroBalanceException("User balance is zero!");
        if (product.getStockCount() < count) throw new InsufficientProductException("Product is insufficient!");
        if (user.getBalance().compareTo(product.getPrice().multiply(BigDecimal.valueOf(count))) < 0)
            throw new InsufficientBalanceException("User balance is insufficient!");
    }

    private void increaseBalance(UserResponse userResponse, Long userId, BigDecimal amount) {
        userResponse.setBalance(userResponse.getBalance().add(amount));
        userService.updateUser(userId, UserMapper.INSTANCE.mapUserResponseToUserRequest(userResponse));
    }

    private void decreaseBalance(UserResponse userResponse, Long userId, BigDecimal amount) {
        userResponse.setBalance(userResponse.getBalance().subtract(amount));
        userService.updateUser(userId, UserMapper.INSTANCE.mapUserResponseToUserRequest(userResponse));
    }

    public void decreaseStockCount(ProductResponse productResponse, Long productId, Integer count) {
        productResponse.setStockCount(productResponse.getStockCount() - count);
        updateProduct(productId, ProductMapper.INSTANCE.mapProductResponseToProductRequest(productResponse));
    }

    public void increaseStockCount(ProductResponse productResponse, Long productId, Integer count) {
        productResponse.setStockCount(productResponse.getStockCount() + count);
        updateProduct(productId, ProductMapper.INSTANCE.mapProductResponseToProductRequest(productResponse));
    }

}
