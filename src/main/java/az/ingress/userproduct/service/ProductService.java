package az.ingress.userproduct.service;

import az.ingress.userproduct.model.request.ProductRequest;
import az.ingress.userproduct.model.request.PurchaseRequest;
import az.ingress.userproduct.model.response.ProductResponse;

import java.util.List;

public interface ProductService {
    Long createProduct(ProductRequest request);

    void updateProduct(Long productId, ProductRequest request);

    void deleteProduct(Long id);

    ProductResponse getProductById(Long id);

    List<ProductResponse> getAllProducts();

    void buyProduct(PurchaseRequest purchaseRequest);

    void returnProduct(PurchaseRequest purchaseRequest);


}
