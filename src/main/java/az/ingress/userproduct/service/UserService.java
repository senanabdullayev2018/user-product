package az.ingress.userproduct.service;

import az.ingress.userproduct.model.request.UserRequest;
import az.ingress.userproduct.model.response.UserResponse;

import java.util.HashMap;
import java.util.List;

public interface UserService {
    Long createUser(UserRequest request);

    void updateUser(Long id, UserRequest request);

    void updateUserActiveStatus(HashMap<String, Boolean> map);

    UserResponse getUserById(Long id);

    UserResponse getUserByUsername(String username);

    List<UserResponse> getUsersByActivityStatus(Boolean isActive);

    List<UserResponse> getAllUsers();
}
