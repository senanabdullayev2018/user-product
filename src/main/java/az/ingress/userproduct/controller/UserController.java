package az.ingress.userproduct.controller;


import az.ingress.userproduct.model.request.UserRequest;
import az.ingress.userproduct.model.response.UserResponse;
import az.ingress.userproduct.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public Long createUser(@RequestBody UserRequest request) {
        return userService.createUser(request);
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable Long id, @RequestBody UserRequest request) {
        userService.updateUser(id, request);
    }

    @PutMapping("/update-user-active-status")
    public void updateUserActiveStatus(@RequestBody HashMap<String, Boolean> map) {
        userService.updateUserActiveStatus(map);
    }

    @GetMapping("/{id}")
    public UserResponse getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/{username}")
    public UserResponse getUserByUsername(@PathVariable String username) {
        return userService.getUserByUsername(username);
    }

    @GetMapping("/activity-status")
    public List<UserResponse> getUsersByActivityStatus(@RequestParam Boolean isActive) {
        return userService.getUsersByActivityStatus(isActive);
    }

    @GetMapping("/all-users")
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers();
    }
}

