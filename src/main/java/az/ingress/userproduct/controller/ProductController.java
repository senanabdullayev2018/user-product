package az.ingress.userproduct.controller;


import az.ingress.userproduct.model.request.ProductRequest;
import az.ingress.userproduct.model.request.PurchaseRequest;
import az.ingress.userproduct.model.response.ProductResponse;
import az.ingress.userproduct.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public Long createProduct(@RequestBody ProductRequest request) {
        return productService.createProduct(request);
    }

    @PutMapping("/{id}")
    public void updateProduct(@PathVariable Long id, @RequestBody ProductRequest request) {
        productService.updateProduct(id, request);
    }

    @DeleteMapping("/{id}")
    public void updateUserActiveStatus(@PathVariable Long id) {
        productService.deleteProduct(id);
    }

    @GetMapping("/{id}")
    public ProductResponse getUserById(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    @GetMapping("/all-products")
    public List<ProductResponse> getAllProducts() {
        return productService.getAllProducts();
    }

    @PostMapping("/buy")
    public void buyProduct(@RequestBody PurchaseRequest request) {
        productService.buyProduct(request);
    }

    @PostMapping("/return")
    public void returnProduct(@RequestBody PurchaseRequest request) {
        productService.returnProduct(request);
    }
}

