package az.ingress.userproduct.dao.repository;

import az.ingress.userproduct.dao.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    List<User> findByIsActiveIs(Boolean isActive);
}
