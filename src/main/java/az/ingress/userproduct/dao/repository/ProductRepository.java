package az.ingress.userproduct.dao.repository;

import az.ingress.userproduct.dao.entity.Product;
import az.ingress.userproduct.dao.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
