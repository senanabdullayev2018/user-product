package az.ingress.userproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserProduct1Application {

	public static void main(String[] args) {
		SpringApplication.run(UserProduct1Application.class, args);
	}

}
