package az.ingress.userproduct.mapper;

import az.ingress.userproduct.dao.entity.User;
import az.ingress.userproduct.model.request.UserRequest;
import az.ingress.userproduct.model.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class UserMapper {
    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "id", ignore = true)
    public abstract User mapUserRequestToUser(UserRequest userRequest);

    public abstract void updateUser(@MappingTarget User user, UserRequest request);

    public abstract UserResponse mapUserToUserResponse(User user);

    public abstract UserRequest mapUserResponseToUserRequest(UserResponse userResponse);

    public abstract List<UserResponse> mapUserListToUserResponseList(List<User> userList);
}
