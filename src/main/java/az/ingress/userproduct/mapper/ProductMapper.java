package az.ingress.userproduct.mapper;

import az.ingress.userproduct.dao.entity.Product;
import az.ingress.userproduct.model.request.ProductRequest;
import az.ingress.userproduct.model.response.ProductResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class ProductMapper {
    public static final ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Mapping(target = "id", ignore = true)
    public abstract Product mapProductRequestToProduct(ProductRequest productRequest);

    public abstract void updateProduct(@MappingTarget Product product, ProductRequest request);

    public abstract ProductResponse mapProductToProductResponse(Product product);

    public abstract ProductRequest mapProductResponseToProductRequest(ProductResponse productResponse);


    public abstract List<ProductResponse> mapProductListToProductResponseList(List<Product> productList);
}
