package az.ingress.userproduct.model.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponse {
    String username;
    String firstname;
    String lastname;
    Integer age;
    BigDecimal balance;
    Boolean isActive;
}
