package az.ingress.userproduct.model.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {
    @NotNull
    String username;
    @NotNull
    String firstname;
    @NotNull
    String lastname;
    @PositiveOrZero
    @NotNull
    Integer age;
    @PositiveOrZero
    @NotNull
    BigDecimal balance;
    @NotNull
    Boolean isActive;
}
