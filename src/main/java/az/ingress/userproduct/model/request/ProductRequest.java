package az.ingress.userproduct.model.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductRequest {
    @NotNull
    String name;
    @NotNull
    @PositiveOrZero
    BigDecimal price;
    @PositiveOrZero
    @NotNull
    Long stockCount;
}
