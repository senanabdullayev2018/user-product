package az.ingress.userproduct.model.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PurchaseRequest {
    @NotNull
    Long userId;
    @NotNull
    Long productId;
    @NotNull
    @Positive
    Integer count;
}
